from datetime import timedelta

from django.shortcuts import render, redirect
from django.utils import timezone
from django.contrib.auth.decorators import login_required, permission_required
from django.conf import settings
from django.http import JsonResponse
from django.core.exceptions import PermissionDenied
from django.db.models import F

from polls.forms import ElectionForm, RegisterForm, PollForm, ElectionListForm, CandidateForm
from .models import PollsUser, Election, ElectionList, Candidate


def index(request):
    context = {'authenticated': 'none'}
    if request.user.is_authenticated and request.user.has_perm('polls.polls_canvote'):
        date_now = timezone.now()
        date_in_1hour = date_now + timedelta(hours=1)
        if request.user.has_perm('polls.polls_canvote_class'):
            elections_ending_qs = Election.objects.filter(date_to__range=[date_now, date_in_1hour])
        else:
            elections_ending_qs = Election.objects.filter(date_to__range=[date_now, date_in_1hour], class_polls=False)

        elections_ending = []
        for election in elections_ending_qs:
            electiond = {
                'pk': election.pk,
                'name': election.name,
                'date_from': election.date_from,
                'date_to': election.date_to,
                'candidates': election.count_candidates(),
                'canvote': election.can_vote(user=request.user),
            }
            elections_ending.append(electiond)
        elections_ending_excludion = elections_ending_qs.values('id')

        if request.user.has_perm('polls.polls_canvote_class'):
            elections_ongoing_qs = Election.objects.filter(date_from__lte=date_now, date_to__gte=date_now) \
                .exclude(id__in=elections_ending_excludion)
        else:
            elections_ongoing_qs = Election.objects.filter(date_from__lte=date_now, date_to__gte=date_now,
                                                           class_polls=False)\
                .exclude(id__in=elections_ending_excludion)

        elections_ongoing = []
        for election in elections_ongoing_qs:
            electiond = {
                'pk': election.pk,
                'name': election.name,
                'date_from': election.date_from,
                'date_to': election.date_to,
                'candidates': election.count_candidates(),
                'canvote': election.can_vote(user=request.user),
            }
            elections_ongoing.append(electiond)

        date_in_1day = date_now + timedelta(days=1)

        if request.user.has_perm('polls.polls_canvote_class'):
            elections_upcoming = Election.objects.filter(date_from__range=[date_now, date_in_1day])
        else:
            elections_upcoming = Election.objects.filter(date_from__range=[date_now, date_in_1day], class_polls=False)

        context.update({
            'elections_ending': elections_ending,
            'elections_ongoing': elections_ongoing,
            'elections_upcoming': elections_upcoming,
        })

    return render(request, 'pages/main.html', context)


def register(request):
    if request.user.is_authenticated:
        return redirect('index')

    context = {'debug': settings.DEBUG}
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            context.update({
                'success': 'Zarejestrowano, aby aktywować konto kliknij w link w mailu. \
                           Przekierowanie nastąpi za 3 sekundy.',
            })
    else:
        form = RegisterForm()

    context.update({
        'form': form,
    })
    return render(request, 'auth/register.html', context)


@login_required(login_url='login')
def profile(request):
    context = {}
    user = PollsUser.objects.get(pk=request.user.pk)
    if request.method == 'POST':
        form = ElectionForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            form = ElectionForm(instance=user)
            context.update({
                'success': 'Poprawnie zapisano dane',
            })
    else:
        form = ElectionForm(instance=user)

    context.update({
        'form': form,
    })
    return render(request, 'pages/profile.html', context)


@login_required(login_url='login')
@permission_required('polls.polls_admin', raise_exception=True)
def create_poll(request):
    context = {}

    if request.method == 'POST':
        form = PollForm(request.POST, user_pk=request.user.pk)
        if form.is_valid():
            poll = form.save()
            form = PollForm(user_pk=request.user.pk)
            context.update({
                'success': 'Dodano wybory, przekierowanie do edycji wyborów za 2 sekundy',
                'poll_id': poll.pk,
            })
    else:
        form = PollForm(user_pk=request.user.pk)

    context.update({
        'form': form
    })

    return render(request, 'polls/create.html', context)


@login_required(login_url='login')
@permission_required('polls.polls_admin', raise_exception=True)
def view_my_polls(request):
    context = {}

    data = Election.objects.filter(owner=request.user)

    if data:
        context.update({
            'data': data,
        })
    else:
        context.update({
            'empty': 'empty',
        })

    return render(request, 'polls/view_my.html', context)


@login_required(login_url='login')
@permission_required('polls.polls_admin', raise_exception=True)
def delete_poll(request, poll_id):
    stat = 'ok'

    try:
        to_del = Election.objects.get(pk=poll_id, owner=request.user)
        to_del.delete()
    except Election.DoesNotExist:
        stat = 'err'

    return JsonResponse({'status': stat})


@login_required(login_url='login')
@permission_required('polls.polls_admin', raise_exception=True)
def edit_poll(request, poll_id):
    context = {'poll_id': poll_id, }

    try:
        election = Election.objects.get(pk=poll_id, owner=request.user)
        form = PollForm(instance=election, edit=True)
        context.update({
            'election': election,
            'form': form,
        })
    except Election.DoesNotExist:
        raise PermissionDenied

    lists = ElectionList.objects.filter(election=election)

    if lists.count() > 0:
        context.update({
            'lists': lists,
        })

    form_new = ElectionListForm(election_pk=poll_id)

    context.update({
        'form_new': form_new,
    })

    return render(request, 'polls/edit.html', context)


@login_required(login_url='login')
@permission_required('polls.polls_admin', raise_exception=True)
def save_edited_poll(request, poll_id):
    stat = 'ok'
    errors = None

    if request.POST:
        try:
            election = Election.objects.get(pk=poll_id, owner=request.user)
            form = PollForm(request.POST, instance=election, edit=True)
            if form.is_valid():
                form.save()
            else:
                stat = 'err'
                errors = form.errors
        except Election.DoesNotExist:
            stat = 'err'
    else:
        stat = 'err'

    resp = {
        'status': stat,
    }

    if errors:
        resp.update({
            'errors': errors
        })

    return JsonResponse(resp)


@login_required(login_url='login')
@permission_required('polls.polls_admin', raise_exception=True)
def create_new_list(request, poll_id):
    stat = 'ok'
    resp = {}
    pk = 0

    if request.POST:
        try:
            Election.objects.get(pk=poll_id, owner=request.user)
            form = ElectionListForm(request.POST, election_pk=poll_id)
            if form.is_valid():
                n_list = form.save()
                pk = n_list.pk
        except Election.DoesNotExist:
            stat = 'err'
    else:
        stat = 'err'

    resp.update({
        'status': stat,
    })

    if stat == 'ok':
        resp.update({
            'pk': pk,
        })

    return JsonResponse(resp)


@login_required(login_url='login')
@permission_required('polls.polls_admin', raise_exception=True)
def delete_poll_list(request, poll_list_id):
    stat = 'ok'

    try:
        to_del = ElectionList.objects.get(pk=poll_list_id)
        if to_del.get_owner().pk == request.user.pk:
            to_del.delete()
        else:
            stat = 'notowner' + to_del.get_owner().username
    except ElectionList.DoesNotExist:
        stat = 'err'

    return JsonResponse({'status': stat})


@login_required(login_url='login')
@permission_required('polls.polls_admin', raise_exception=True)
def edit_poll_list(request, poll_list_id):
    back_id = request.GET.get('back', None)
    context = {
        'poll_list_id': poll_list_id,
        'back_id': back_id,
    }
    election_list = None

    try:
        election_list = ElectionList.objects.get(pk=poll_list_id)
        if election_list.get_owner().pk != request.user.pk:
            raise PermissionDenied
        form = ElectionListForm(instance=election_list, edit=True)

        context.update({
            'election_list': election_list,
            'form': form,
        })
    except ElectionList.DoesNotExist:
        raise PermissionDenied

    candidates = Candidate.objects.filter(election_list=election_list)

    if candidates.count() > 0:
        context.update({
            'candidates': candidates,
        })

    form_new = CandidateForm(edit=True)

    context.update({
        'form_new': form_new,
    })

    return render(request, 'polls/edit_list.html', context)


@login_required(login_url='login')
@permission_required('polls.polls_admin', raise_exception=True)
def save_edited_poll_list(request, poll_list_id):
    stat = 'ok'

    if request.POST:
        try:
            election_list = ElectionList.objects.get(pk=poll_list_id)
            if election_list.get_owner().pk != request.user.pk:
                stat = 'err'
            else:
                form = ElectionListForm(request.POST, instance=election_list, edit=True)
                if form.is_valid():
                    form.save()
                else:
                    stat = 'err'
                if form.is_valid():
                    form.save()
                else:
                    stat = 'err'
        except ElectionList.DoesNotExist:
            stat = 'err'
    else:
        stat = 'err'

    resp = {
        'status': stat,
    }

    return JsonResponse(resp)


@login_required(login_url='login')
@permission_required('polls.polls_admin', raise_exception=True)
def create_new_candidate(request, poll_list_id):
    stat = 'ok'
    resp = {}

    if request.POST:
        try:
            election_list = ElectionList.objects.get(pk=poll_list_id)
            if election_list.get_owner().pk != request.user.pk:
                stat = 'err'
            else:
                form = CandidateForm(request.POST, election_list_pk=poll_list_id)
                if form.is_valid():
                    form.save()
                else:
                    status = 'err'
        except ElectionList.DoesNotExist:
            stat = 'err'
    else:
        stat = 'err'

    resp.update({
        'status': stat,
    })

    return JsonResponse(resp)


@login_required(login_url='login')
@permission_required('polls.polls_admin', raise_exception=True)
def delete_candidate(request, candidate_id):
    stat = 'ok'
    resp = {}

    try:
        to_del = Candidate.objects.get(pk=candidate_id)
        if to_del.get_owner().pk == request.user.pk:
            to_del.delete()
        else:
            stat = 'err'
    except Candidate.DoesNotExist:
        stat = 'err'

    resp.update({
        'status': stat,
    })

    return JsonResponse(resp)


@login_required(login_url='login')
@permission_required('polls.polls_admin', raise_exception=True)
def edit_candidate(request, candidate_id):
    back_list_id = request.GET.get('backlist', None)
    back_election_id = request.GET.get('backelection', None)
    context = {
        'back_list_id': back_list_id,
        'back_election_id': back_election_id,
    }

    try:
        candidate = Candidate.objects.get(pk=candidate_id)
        if candidate.get_owner().pk != request.user.pk:
            raise PermissionDenied

        form = CandidateForm(instance=candidate, edit=True)

        context.update({
            'candidate': candidate,
            'form': form,
        })
    except Candidate.DoesNotExist:
        raise PermissionDenied

    return render(request, 'polls/edit_candidate.html', context)


@login_required(login_url='login')
@permission_required('polls.polls_admin', raise_exception=True)
def save_edited_candidate(request, candidate_id):
    stat = 'ok'

    if request.POST:
        try:
            candidate = Candidate.objects.get(pk=candidate_id)
            if candidate.get_owner().pk != request.user.pk:
                stat = 'err'
            else:
                form = CandidateForm(request.POST, instance=candidate, edit=True)
                if form.is_valid():
                    form.save()
                else:
                    stat = 'err'
        except ElectionList.DoesNotExist:
            stat = 'err'
    else:
        stat = 'err'

    resp = {
        'status': stat,
    }

    return JsonResponse(resp)


@login_required(login_url='login')
@permission_required('polls.polls_canvote', raise_exception=True)
def vote(request, election_id):
    context = {
        'election_id': election_id,
    }

    el = None

    if request.POST:
        value = request.POST.get('candidate')
        candidate = None
        try:
            candidate = Candidate.objects.get(pk=value)
        except Candidate.DoesNotExist:
            context.update({
                'election_error': 'Brak takiego kandydata',
            })
        user = PollsUser.objects.get(pk=request.user.pk)
        candidate.get_election().voters.add(user)
        candidate.votes = F('votes') + 1
        candidate.save()

    try:
        el = Election.objects.get(pk=election_id)
    except Election.DoesNotExist:
        context.update({
            'election_error': 'Nie ma takich wyborów',
        })

    if el is not None:
        ellists = ElectionList.objects.filter(election=el)
        lists = {}

        for clist in ellists:
            lists.update({clist.name: Candidate.objects.filter(election_list=clist)})

        if el.class_polls and not request.user.has_perm('polls.polls_canvote_class'):
            context.update({
                'election_error': 'Brak uprawnień do głosowania, najpierw dodaj numer klasy.',
                'show': True,
                'noform': True,
            })
        elif not el.started():
            context.update({
                'lists': lists,
                'election_error': 'Wybory jeszcze się nie rozpoczęły, to tylko podgląd kandydatów.',
                'show': True,
                'noform': True,
            })
        elif el.ended():
            context.update({
                'lists': lists,
                'election_error': 'Wybory się zakończyły, to tylko podgląd kandydatów.',
                'show': True,
                'noform': True,
            })
        elif el.voted(user=request.user):
            context.update({
                'election_error': 'Już głosowałeś w tych wyborach.',
                'lists': lists,
                'show': True,
                'noform': True,
            })
        else:
            context.update({
                'lists': lists,
                'show': True,
                'noform': False,
            })

    return render(request, 'polls/vote.html', context)


@login_required(login_url='login')
@permission_required('polls.polls_admin', raise_exception=True)
def results(request, election_id):
    context = {
        'election_id': election_id,
    }

    el = None
    try:
        el = Election.objects.get(pk=election_id)
    except Election.DoesNotExist:
        context.update({
            'election_error': 'Nie ma takich wyborów',
        })

    if el is not None:
        ellists = ElectionList.objects.filter(election=el)
        lists = {}

        for clist in ellists:
            lists.update({clist.name: Candidate.objects.filter(election_list=clist)})

        context.update({
            'lists': lists,
        })

    return render(request, 'polls/results.html', context)
