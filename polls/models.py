from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User


class PollsUser(User):
    def __str__(self):
        return self.first_name + ' ' + self.last_name + ' ' + self.username

    birth_date = models.DateField(null=True, blank=True)
    address = models.CharField(max_length=100, null=True, blank=True)
    pesel = models.CharField(max_length=11, null=True, blank=True)
    class_number = models.CharField(max_length=3, null=True, blank=True)

    class Meta:
        permissions = (
            ('polls_admin', 'Może tworzyć wybory'),
            ('polls_canvote', 'Może uczestniczyć w wyborach'),
            ('polls_canvote_class', 'Może uczestniczyć w wyborach klasowych'),
        )


class Election(models.Model):
    def __str__(self):
        return self.name

    def count_candidates(self):
        els = ElectionList.objects.filter(election=self)
        count = 0
        for el in els:
            count += el.count_candidates()

        return count

    def ongoing(self):
        now = timezone.localtime(timezone.now())

        if self.date_from <= now <= self.date_to:
            return True

        return False

    def started(self):
        now = timezone.localtime(timezone.now())

        if self.date_from <= now:
            return True

        return False

    def ended(self):
        now = timezone.localtime(timezone.now())

        if self.date_to < now:
            return True

        return False

    def voted(self, user=None):
        return self.voters.filter(pk=user.pk).exists()

    def can_vote(self, user=None):
        if user is None:
            return False

        if self.class_polls and not user.has_perm('polls.polls_canvote_class'):
            return False

        voted = self.voters.filter(pk=user.pk).exists()

        if voted:
            return False

        return True

    owner = models.ForeignKey(
        'PollsUser',
        on_delete=models.CASCADE,
        related_name='owner'
    )
    name = models.CharField(max_length=30)
    date_from = models.DateTimeField()
    date_to = models.DateTimeField()
    class_polls = models.BooleanField(default=False)
    general_visible = models.BooleanField(default=False)
    precise_visible = models.BooleanField(default=False)
    refresh_threshold = models.IntegerField(default=10)
    voters = models.ManyToManyField('PollsUser', related_name='voters')


class ElectionList(models.Model):
    def __str__(self):
        return self.name

    name = models.CharField(max_length=60)
    election = models.ForeignKey(
        'Election',
        on_delete=models.CASCADE,
    )

    def count_candidates(self):
        return Candidate.objects.filter(election_list=self).count()

    def get_owner(self):
        return self.election.owner


class Candidate(models.Model):
    def __str__(self):
        return self.name

    name = models.CharField(max_length=60)
    election_list = models.ForeignKey(
        'ElectionList',
        on_delete=models.CASCADE,
    )
    votes = models.IntegerField(default=0)

    def get_owner(self):
        return self.election_list.election.owner

    def get_election(self):
        return self.election_list.election
