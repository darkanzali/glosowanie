from django import template

register = template.Library()


@register.filter
def set_placeholder(field, placeholder=None):
    if placeholder is None:
        return field
    field.field.widget.attrs.update({"placeholder": placeholder})
    return field


@register.filter
def set_css(field, css=None):
    if css is None:
        return field
    return field.as_widget(attrs={"class": css})
