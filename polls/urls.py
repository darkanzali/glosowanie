from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    # Account management >>>
    url(r'^$', views.index, name='index'),
    url(r'^login/$', auth_views.login, {'template_name': 'auth/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^register/$', views.register, name='register'),
    url(r'^profile/$', views.profile, name='user'),
    # Account management <<<

    # Polls admin >>>
    url(r'^create/$', views.create_poll, name='create_poll'),
    url(r'^view_my_polls/$', views.view_my_polls, name='view_my_polls'),
    url(r'^delete_poll/(?P<poll_id>\d+)$', views.delete_poll, name='delete_poll'),
    url(r'^edit_poll/(?P<poll_id>\d+)$', views.edit_poll, name='edit_poll'),
    url(r'^save_edited_poll/(?P<poll_id>\d+)$', views.save_edited_poll, name='save_edited_poll'),
    url(r'^create_new_list/(?P<poll_id>\d+)$', views.create_new_list, name='create_new_list'),
    url(r'^delete_poll_list/(?P<poll_list_id>\d+)$', views.delete_poll_list, name='delete_poll_list'),
    url(r'^edit_poll_list/(?P<poll_list_id>\d+)$', views.edit_poll_list, name='edit_poll_list'),
    url(r'^save_edited_poll_list/(?P<poll_list_id>\d+)$', views.save_edited_poll_list, name='save_edited_poll_list'),
    url(r'^create_new_candidate/(?P<poll_list_id>\d+)$', views.create_new_candidate, name='create_new_candidate'),
    url(r'^delete_candidate/(?P<candidate_id>\d+)$', views.delete_candidate, name='delete_candidate'),
    url(r'^edit_candidate/(?P<candidate_id>\d+)$', views.edit_candidate, name='edit_candidate'),
    url(r'^save_edited_candidate/(?P<candidate_id>\d+)$', views.save_edited_candidate, name='save_edited_candidate'),
    # Polls admin <<<

    # Polls voting >>>
    url(r'^vote/(?P<election_id>\d+)$', views.vote, name='vote'),
    url(r'^results/(?P<election_id>\d+)$', views.results, name='results'),
    # Polls voting <<<
]
