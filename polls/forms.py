import json
import urllib
from urllib import parse, request
from datetime import date, datetime
from dateutil.relativedelta import relativedelta

from django.conf import settings
from django.contrib.auth.models import Permission
from django.core.validators import validate_email
from django.utils import timezone

from django import forms
from .models import PollsUser, Election, ElectionList, Candidate


class RegisterForm(forms.ModelForm):
    username = forms.CharField(label='Login', max_length=150, widget=forms.TextInput(
            attrs={
                'class': 'input-top form-control',
                'placeholder': 'Twój nick',
            }))

    first_name = forms.CharField(label='Imię', max_length=150, widget=forms.TextInput(
            attrs={
                'class': 'input-middle form-control',
                'placeholder': 'Imię',
            }))

    last_name = forms.CharField(label='Nazwisko', max_length=150, widget=forms.TextInput(
            attrs={
                'class': 'input-middle form-control',
                'placeholder': 'Nazwisko',
            }))

    email = forms.CharField(label='E-mail', max_length=254, widget=forms.EmailInput(
            attrs={
                'class': 'input-middle form-control',
                'placeholder': 'Adres e-mail',
            }))

    password = forms.CharField(label='Hasło', widget=forms.PasswordInput(
            attrs={
                'class': 'input-middle form-control',
                'placeholder': 'Hasło'
            }))

    password_confirm = forms.CharField(label="Potwierdź hasło", widget=forms.PasswordInput(
            attrs={
                'class': 'input-bottom form-control',
                'placeholder': 'Potwierdź hasło'
            }))

    captcha = forms.CharField(widget=forms.HiddenInput(), required=False)

    class Meta:
        model = PollsUser
        fields = ('username', 'first_name', 'last_name', 'email', 'password', 'password_confirm')

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.username = self.cleaned_data['username']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        user.set_password(self.cleaned_data['password'])

        if commit:
            user.save()

        return user

    def clean_username(self):
        username = self.cleaned_data['username']

        username_is_email = False
        try:
            validate_email(self.cleaned_data['username'])
            username_is_email = True
        except forms.ValidationError:
            pass

        if username_is_email:
            raise forms.ValidationError('Nick nie może być adresem email')

        return username

    def clean_password(self):
        password = self.cleaned_data['password']

        if len(password) == 0:
            raise forms.ValidationError('Pole hasła nie może być puste')

        return password

    def clean_password_confirm(self):
        password_confirm = self.cleaned_data['password_confirm']

        if len(password_confirm) == 0:
            raise forms.ValidationError('Pole potwierdzenia hasła nie może być puste')

        return password_confirm

    def clean_captcha(self):
        captcha = self.cleaned_data['captcha']

        error = False

        if not settings.DEBUG and not settings.TESTING:
            url = 'https://www.google.com/recaptcha/api/siteverify'
            values = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': captcha
            }
            data = urllib.parse.urlencode(values).encode()
            req = urllib.request.Request(url, data=data)
            response = urllib.request.urlopen(req)
            result = json.loads(response.read().decode())

            if not result['success']:
                error = True
        elif captcha == 'invalid_captcha':
            error = True

        if error:
            raise forms.ValidationError('Błąd walidacji captchy')

        return captcha

    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()

        username = cleaned_data.get('username')
        email = cleaned_data.get('email')
        password = cleaned_data.get('password')
        passwordc = cleaned_data.get('password_confirm')

        if password and passwordc:
            if password != passwordc:
                raise forms.ValidationError('Hasła się nie zgadzają')

        if username and email:
            if PollsUser.objects.filter(email=email).exists():
                raise forms.ValidationError('Użytkownik o takim adresie email już istnieje')


class ElectionForm(forms.ModelForm):
    first_name = forms.CharField(label='Imie', max_length=150, widget=forms.TextInput(
        attrs={
            'class': 'input-top form-control',
            'placeholder': 'Imie',
            'autocomplete': 'off',
        }),
        required=True)
    last_name = forms.CharField(label='Nazwisko', max_length=150, widget=forms.TextInput(
        attrs={
            'class': 'input-middle form-control',
            'placeholder': 'Nazwisko',
            'autocomplete': 'off',
        }),
        required=True)
    birth_date = forms.DateField(label='Data urodzenia', widget=forms.DateInput(
            attrs={
                'class': 'input-middle form-control',
                'placeholder': 'Data urodzenia',
                'autocomplete': 'off',
            },
            format='%Y/%m/%d'),
        input_formats=('%Y/%m/%d',),
        required=False)
    address = forms.CharField(label='Adres', max_length=150, widget=forms.TextInput(
        attrs={
            'class': 'input-middle form-control',
            'placeholder': 'Adres',
            'autocomplete': 'off',
        }),
        required=False)
    pesel = forms.CharField(label='Pesel', max_length=150, widget=forms.TextInput(
        attrs={
            'class': 'input-middle form-control',
            'placeholder': 'Pesel',
            'autocomplete': 'off',
        }),
        required=False)
    class_number = forms.CharField(label='Numer klasy', max_length=150, widget=forms.TextInput(
        attrs={
            'class': 'input-bottom form-control',
            'placeholder': 'Numer klasy',
            'autocomplete': 'off',
        }),
        required=False)

    class Meta:
        model = PollsUser
        fields = ('first_name', 'last_name', 'birth_date', 'address', 'pesel', 'class_number')

    def clean_birth_date(self):
        birth_date = self.cleaned_data['birth_date']

        if birth_date is not None:
            if date.today() < birth_date:
                raise forms.ValidationError("Data urodzenia musi być w przeszłości.")

        return birth_date

    def clean_pesel(self):
        pesel = self.cleaned_data['pesel']

        bad_pesel = False

        if len(pesel) == 0:
            pass
        elif len(pesel) != 11:
            bad_pesel = True
        else:
            factor = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1]
            res = 0
            for i, c in enumerate(pesel):
                num = 0
                try:
                    num = int(c)
                except ValueError:
                    bad_pesel = True

                res += num * factor[i]

            if res % 10 != 0:
                bad_pesel = True

        if bad_pesel:
            raise forms.ValidationError('Niepoprawny numer pesel')

        return pesel

    def clean_class_number(self):
        class_number = self.cleaned_data['class_number']

        if len(class_number) > 3:
            raise forms.ValidationError('Numer klasy może być maksymalnie 3 znakowy')

        return class_number

    def save(self, commit=True):
        user = super(ElectionForm, self).save(commit=False)
        first_name = self.cleaned_data['first_name']
        last_name = self.cleaned_data['last_name']
        birth_date = self.cleaned_data['birth_date']
        address = self.cleaned_data['address']
        pesel = self.cleaned_data['pesel']
        class_number = self.cleaned_data['class_number']

        polls_admin = Permission.objects.get(codename='polls_admin')
        polls_canvote = Permission.objects.get(codename='polls_canvote')
        polls_canvote_class = Permission.objects.get(codename='polls_canvote_class')

        if first_name and last_name and birth_date and address and pesel:
            user.user_permissions.add(polls_admin)

            if birth_date <= date.today() - relativedelta(years=6):
                user.user_permissions.add(polls_canvote)
            else:
                user.user_permissions.remove(polls_canvote)
        else:
            user.user_permissions.remove(polls_admin)
            user.user_permissions.remove(polls_canvote)

        if class_number:
            user.user_permissions.add(polls_canvote_class)
        else:
            user.user_permissions.remove(polls_canvote_class)

        if commit:
            user.save()

        return user


class PollForm(forms.ModelForm):
    name = forms.CharField(label='Nazwa wyborów', max_length=150, widget=forms.TextInput(
        attrs={
            'class': 'input-top form-control',
            'placeholder': 'Nazwa wyborów',
            'autocomplete': 'off',
        }))
    date_from = forms.DateTimeField(label='Data rozpoczęcia', widget=forms.DateInput(
        attrs={
            'class': 'input-middle form-control',
            'placeholder': 'Data rozpoczęcia',
            'autocomplete': 'off',
        },
        format='%Y/%m/%d %H:%M'),
        input_formats=settings.DATETIME_INPUT_FORMATS,
        )
    date_to = forms.DateTimeField(label='Data zakończenia', widget=forms.DateInput(
        attrs={
            'class': 'input-middle form-control',
            'placeholder': 'Data zakończenia',
            'autocomplete': 'off',
        },
        format='%Y/%m/%d %H:%M'),
        input_formats=settings.DATETIME_INPUT_FORMATS,
        )
    class_polls = forms.BooleanField(label="Wybory szkolne", widget=forms.CheckboxInput(
        attrs={
            'class': 'input-middle form-control',
            'placeholder': 'Wybory szkolne',
        }),
        required=False)
    general_visible = forms.BooleanField(label="Ogólne statystyki", widget=forms.CheckboxInput(
        attrs={
            'class': 'input-middle form-control',
            'placeholder': 'Nazwa wyborów',
        }),
        required=False)
    precise_visible = forms.BooleanField(label="Precyzyjne statystyki", widget=forms.CheckboxInput(
        attrs={
            'class': 'input-middle form-control',
            'placeholder': 'Prezyzyjne statystyki',
        }),
        required=False)
    refresh_threshold = forms.IntegerField(label="Próg odświeżenia", widget=forms.NumberInput(
        attrs={
            'class': 'input-bottom form-control',
            'placeholder': 'Prezyzyjne statystyki',
        }),
        initial=10)

    class Meta:
        model = Election
        fields = ('name', 'date_from', 'date_to', 'class_polls',
                  'general_visible', 'precise_visible', 'refresh_threshold')

    def __init__(self, *args, **kwargs):
        self.edit = kwargs.pop('edit', None)
        if not self.edit:
            self.user_pk = kwargs.pop('user_pk')

        super(PollForm, self).__init__(*args, **kwargs)

        if not self.edit:
            self.initial['date_from'] = datetime.now().strftime('%Y/%m/%d %H:%M')
            self.initial['date_to'] = (datetime.now() + relativedelta(days=1)).strftime('%Y/%m/%d %H:%M')

    def clean(self):
        super(PollForm, self).clean()

        date_from = self.cleaned_data['date_from']
        date_to = self.cleaned_data['date_to']

        if date_from >= date_to:
            raise forms.ValidationError("Data zakończenia musi być późniejsza od rozpoczęcia")

        if date_to <= timezone.now():
            raise forms.ValidationError("Data zakończenia musi być w przyszłości")

    def save(self, commit=True):
        polls = super(PollForm, self).save(commit=False)

        if not self.edit:
            user = PollsUser.objects.get(pk=self.user_pk)
            polls.owner = user

        if commit:
            polls.save()

        return polls


class ElectionListForm(forms.ModelForm):
    name = forms.CharField(label='Nazwa listy', max_length=150, widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Nazwa listy',
            'autocomplete': 'off',
        }))

    class Meta:
        model = ElectionList
        fields = ('name',)

    def __init__(self, *args, **kwargs):
        self.edit = kwargs.pop('edit', None)
        if not self.edit:
            self.election_pk = kwargs.pop('election_pk')
        super(ElectionListForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        nlist = super(ElectionListForm, self).save(commit=False)

        if not self.edit:
            election = Election.objects.get(pk=self.election_pk)
            nlist.election = election

        if commit:
            nlist.save()

        return nlist


class CandidateForm(forms.ModelForm):
    name = forms.CharField(label='Imię i nazwisko kandydata', max_length=150, widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Imię i nazwisko kandydata',
            'autocomplete': 'off',
        }))

    class Meta:
        model = Candidate
        fields = ('name',)

    def __init__(self, *args, **kwargs):
        self.edit = kwargs.pop('edit', None)
        if not self.edit:
            self.election_list_pk = kwargs.pop('election_list_pk')
        super(CandidateForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        ncandidate = super(CandidateForm, self).save(commit=False)

        if not self.edit:
            election_list = ElectionList.objects.get(pk=self.election_list_pk)
            ncandidate.election_list = election_list

        if commit:
            ncandidate.save()

        return ncandidate
