from django.test import TestCase
from django.contrib.auth import authenticate

from polls.forms import RegisterForm
from polls.models import PollsUser


class UserRegisterFormTests(TestCase):

    def test_form_correct(self):
        """
        Simply registration
        """
        form = RegisterForm(data={
            'username': 'test',
            'first_name': 'imie',
            'last_name': 'nazwisko',
            'email': 'email@email.com',
            'password': 'testpassword',
            'password_confirm': 'testpassword',
            'captcha': 'dummy'}
        )
        self.assertIs(form.is_valid(), True)

    def test_form_email_in_username(self):
        """
        Email in username
        """
        form = RegisterForm(data={
            'username': 'email@email.com',
            'first_name': 'imie',
            'last_name': 'nazwisko',
            'email': 'email@email.com',
            'password': 'testpassword',
            'password_confirm': 'testpassword',
            'captcha': 'dummy'}
        )
        self.assertIs(form.is_valid(), False)

    def test_form_incorrect_email(self):
        """
        Incorrect email
        """
        form = RegisterForm(data={
            'username': 'test',
            'first_name': 'imie',
            'last_name': 'nazwisko',
            'email': 'wrong_email',
            'password': 'testpassword',
            'password_confirm': 'testpassword',
            'captcha': 'dummy'}
        )
        self.assertIs(form.is_valid(), False)

    def test_form_passwords_not_match(self):
        """
        Difference in password fields
        """
        form = RegisterForm(data={
            'username': 'test',
            'first_name': 'imie',
            'last_name': 'nazwisko',
            'email': 'email@email.com',
            'password': 'testpassword',
            'password_confirm': 'differenttestpassword',
            'captcha': 'dummy'}
        )
        self.assertIs(form.is_valid(), False)

    def test_form_invalid_captcha(self):
        """
        Invalid captcha
        """
        form = RegisterForm(data={
            'username': 'test',
            'first_name': 'imie',
            'last_name': 'nazwisko',
            'email': 'email@email.com',
            'password': 'testpassword',
            'password_confirm': 'testpassword',
            'captcha': 'invalid_captcha'}
        )
        self.assertIs(form.is_valid(), False)

    def test_form_existing_user_username(self):
        """
        Existing user, username
        """
        form = RegisterForm(data={
            'username': 'test',
            'first_name': 'imie',
            'last_name': 'nazwisko',
            'email': 'email@email.com',
            'password': 'testpassword',
            'password_confirm': 'testpassword',
            'captcha': 'dummy'}
        )
        self.assertIs(form.is_valid(), True)
        form.save()
        form2 = RegisterForm(data={
            'username': 'test',
            'first_name': 'imie',
            'last_name': 'nazwisko',
            'email': 'email2@email.com',
            'password': 'testpassword',
            'password_confirm': 'testpassword',
            'captcha': 'dummy'}
        )
        self.assertIs(form2.is_valid(), False)

    def test_form_existing_user_email(self):
        """
        Existing user, email
        """
        form = RegisterForm(data={
            'username': 'test',
            'first_name': 'imie',
            'last_name': 'nazwisko',
            'email': 'email@email.com',
            'password': 'testpassword',
            'password_confirm': 'testpassword',
            'captcha': 'dummy'}
        )
        self.assertIs(form.is_valid(), True)
        form.save()
        form2 = RegisterForm(data={
            'username': 'test2',
            'first_name': 'imie',
            'last_name': 'nazwisko',
            'email': 'email@email.com',
            'password': 'testpassword',
            'password_confirm': 'testpassword',
            'captcha': 'dummy'}
        )
        self.assertIs(form2.is_valid(), False)


class UserLoginFormTests(TestCase):

    def setUp(self):
        """
        Setup user
        """
        form = RegisterForm(data={
            'username': 'test',
            'first_name': 'imie',
            'last_name': 'nazwisko',
            'email': 'email@email.com',
            'password': 'testpassword',
            'password_confirm': 'testpassword',
            'captcha': 'dummy'}
        )
        if form.is_valid():
            form.save()

    def test_form_existing_user_username(self):
        """
        Test login with username
        """
        user_login = 'test'
        password = 'testpassword'

        username = None

        if PollsUser.objects.filter(username=user_login).exists():
            username = user_login
        elif PollsUser.objects.filter(email=user_login).exists():
            username = PollsUser.objects.filter(email=user_login).first().username
        else:
            pass

        self.assertEqual(username, user_login)

        authenticated = False
        if username is not None:
            user = authenticate(username=username, password=password)
            if user is not None:
                authenticated = True
            else:
                authenticated = False

        self.assertIs(authenticated, True)

    def test_form_existing_user_email(self):
        """
        Test login with username
        """
        user_name = 'test'  # Correct username

        user_login = 'email@email.com'
        password = 'testpassword'

        username = None

        if PollsUser.objects.filter(username=user_login).exists():
            username = user_login
        elif PollsUser.objects.filter(email=user_login).exists():
            username = PollsUser.objects.filter(email=user_login).first().username
        else:
            pass

        self.assertEqual(username, user_name)

        authenticated = False
        if username is not None:
            user = authenticate(username=username, password=password)
            if user is not None:
                authenticated = True
            else:
                authenticated = False

        self.assertIs(authenticated, True)

    def test_form_not_existing_user(self):
        """
        Test login with username
        """
        user_login = 'not_existing_user'
        password = 'testpassword'

        username = None

        if PollsUser.objects.filter(username=user_login).exists():
            username = user_login
        elif PollsUser.objects.filter(email=user_login).exists():
            username = PollsUser.objects.filter(email=user_login).first().username
        else:
            pass

        self.assertIs(username, None)

        authenticated = False
        if username is not None:
            user = authenticate(username=username, password=password)
            if user is not None:
                authenticated = True
            else:
                authenticated = False

        self.assertIs(authenticated, False)
